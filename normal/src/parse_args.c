/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define _GNU_SOURCE
#include <getopt.h>

#include <debug/sahtrace.h>

#include <pcb/core.h>
#include <pcb/utils/privilege.h>

#include "parse_args.h"

static const plugin_config_t appconfig_default = {
    .trace_level = 0,
    .trace_output = TRACE_TYPE_SYSLOG,
    .ipc_socket_name = "",
    .tcp_address = "",
    .tcp_port = "",
    .uri = "",
    .name = "pcb_plugin",
    .busname = NULL,
    .ns = "",
    .configfile = "",
    .sofile = "",
    .pcb = NULL,
    .system_bus = NULL,
    .foreground = false,
    .exit_app = false,
    .sync = false,
    .plugin_argc = 0,
    .plugin_argv = NULL,
    .certificate = NULL,
    .ca = NULL,
    .priority = 0,
    .ssl_connection = true,
    .block_signals = true,
    .privileges = {
        .enabled = false // the default.
    }
};
plugin_config_t appconfig;

void clear_config(plugin_config_t* config) {
    *config = appconfig_default;
}

string_list_t sofiles;
argument_value_list_t arguments;

typedef struct _options {
    char sopt;
    const char* lopt;
    bool needArgument;
} options_t;

static options_t options[] = {
    { 'h', "help", false },
    { 'v', "verbose", false },
    { 'f', "foreground", false },
    { 'o', "output", true },
    { 'I', "IPC", true },
    { 'A', "tcp_address", true },
    { 'P', "tcp_port", true },
    { 'u', "uri", true },
    { 'n', "name", true },
    { 'N', "namespace", true },
    { 'c', "config", true },
    { 's', "sofile", true },
#ifdef OPEN_SSL_SUPPORT
    { 'C', "certificate", true },
    { 'R', "root", true },
    { 'l', "link_ssl_only", false },
#endif
    { 'p', "priority", true },
    { 'S', "sync", false },
    { 'd', "do_not_block_signals", false },
    { 'a', "args", false },
    { 'U', "priv_auto", true },
    { 'V', "priv_user", true },
    { 'W', "priv_group", true },
    { 'X', "priv_cap_retain", true },
    { 0, NULL, false }
};

static void print_help(const char* appname) {
    printf("\n");
    printf("Usage: %s [OPTIONS] [ARG1=VALUE ARG2=VALUE ARGn=VALUE] [SOFILES]\n", appname);
    printf("Start a PCB application\n");
    printf("\n");
    printf("Mandatory arguments to long options are mandatory for short options too.\n");
    printf("\nOPTIONS\n");
    printf("   -h, --help                     Print this help\n");
    printf("   -v, --verbose[=level]          Increase the debug output level\n");
    printf("   -f, --foreground               Keep in foreground\n");
    printf("   -o, --output=stdout|syslog     Trace output destination\n");
    printf("   -I  --ipc=name                 IPC listen socket name\n");
    printf("   -A  --tcp_address=address      TCP listen socket address\n");
    printf("   -P  --tcp_port=port            TCP listen socket port\n");
    printf("   -u  --uri=uri                  URI of bus or plugin to connect to\n");
    printf("   -n  --name=name                Connection name, used in debugging information\n");
    printf("   -N  --namespace=name           Namespace\n");
    printf("   -c  --config=file              File containing data model definition\n");
    printf("   -s  --sofile=name              Filename of shared object to load (multiple of this option can be specified)\n");
#ifdef OPEN_SSL_SUPPORT
    printf("   -C  --certificate=file         File containing certificate and certificate chain (PEM format)\n");
    printf("   -R  --root=file                File containing trusted root certificate(s) (PEM format)\n");
    printf("   -l  --link_ssl_only            Don't create a ssl connection, only link to ssl\n");
#endif
    printf("   -p  --priority=PRIOR           Set the nice value\n");
    printf("   -S  --sync                     Synchronize with the Process object from the bus\n");
    printf("   -d  --do_not_block_signals     By default %s blocks a set of signals, this option will disable the default behavior\n", appname);
    printf("   -a  --args                     Start of arguments that has to be passed to the plugin\n");
    printf("\n");
    printf("   -U  --priv_auto=on|off         (default on) Unless this is explicitly 'off', any other priv_ argument enables privilege-dropping\n");
    printf("   -V  --priv_user=username       After pcb_app_initialize, set process user to this user\n");
    printf("   -W  --priv_group=groupname     After pcb_app_initialize, set process group to this group\n");
    printf("   -X  --priv_cap_retain=<cap1>,<cap2>,...,<capN> After pcb_app_initialize, retain these capabilities (note: no spaces!)\n");
    printf("\n");
}

typedef struct parse_option_state_t {
    int pos;
    const char* option;
    int index;
} parse_option_state_t;

static void parse_option_state_init(parse_option_state_t* state) {
    memset(state, 0, sizeof(*state));
    state->pos = 0;
    state->option = NULL;
    state->index = 1;
}

static int parse_option(parse_option_state_t* parse_state, int argc, char* const argv[], int* current_index, const char** argument) {

    *current_index = parse_state->index;
    char foundopt = 0;
    bool lastshort = false;
    bool longoption = false;
    *argument = NULL;
    do {
        if(parse_state->index >= argc) {
            // end of argument list
            return -1;
        }

        parse_state->option = argv[parse_state->index];

        if((parse_state->option[0] == '-') && (parse_state->option[1] == '-')) {
            // long option
            parse_state->index++;
            longoption = true;
            *argument = strchr(parse_state->option, '=');
            if(*argument) {
                (*argument)++;
            }
            break;
        } else if(parse_state->option[0] == '-') {
            // short option
            longoption = false;
            parse_state->pos++;
            foundopt = parse_state->option[parse_state->pos];
            if(!parse_state->option[parse_state->pos + 1]) {
                parse_state->pos = 0;
                lastshort = true;
                parse_state->index++;
            }
            break;
        } else {
            *argument = parse_state->option;
            parse_state->index++;
            return 0;
        }
    } while(1);

    int i = 0;
    for(i = 0; options[i].sopt; i++) {
        if(longoption) {
            if(strncmp(options[i].lopt, parse_state->option + 2, (size_t) (*argument - (parse_state->option + 2) - 1)) == 0) {
                break;
            }
        } else {
            if(options[i].sopt == foundopt) {
                break;
            }
        }
    }

    if(options[i].sopt == 0) {
        *argument = parse_state->option;
        return 0;
    }


    if(options[i].sopt && options[i].needArgument && !*argument) {
        if(!longoption && !lastshort) {
            *argument = parse_state->option + parse_state->pos + 1;
            parse_state->index++;
            parse_state->pos = 0;
        } else {
            if((parse_state->index < argc) && (argv[parse_state->index][0] != '-')) {
                *argument = argv[parse_state->index];
                parse_state->index++;
            } else {
                return -2;
            }
        }
    }

    return options[i].sopt;
}

static void parse_argument(const char* argument) {
    char* value = strchr(argument, '=');
    if(value) {
        // is a setting
        value[0] = 0;
        value++;
        variant_t var;
        variant_initialize(&var, variant_type_string);
        variant_setChar(&var, value);
        argument_valueAdd(&arguments, argument, &var);
        variant_cleanup(&var);
    } else {
        // is a so file
        string_list_iterator_t* it = string_list_iterator_createFromChar(argument);
        string_list_append(&sofiles, it);
    }
}

static bool parse_capabilities_into(priv_cap_t* target, const char* csv_capability_names) {
    const bool initialized = priv_cap_initialize(target);
    assert(initialized);
    char* csv_clone = strdup(csv_capability_names);
    for(const char* cap_str = strtok(csv_clone, ",");
        cap_str;
        cap_str = strtok(NULL, ",")) {
        const uint32_t cap = priv_cap_fromString(cap_str);
        if(cap == 0xFFFFFFFF) {
            free(csv_clone);
            return false;
        }
        priv_cap_set(target, cap);
    }
    free(csv_clone);
    return true;
}

int parse_arguments(int argc, char* const argv[]) {
    int arg = 0;
    const char* optarg = NULL;
    int current_index = 0;

    string_list_initialize(&sofiles);
    argument_value_list_initialize(&arguments);
    clear_config(&appconfig);

    parse_option_state_t parse_state;
    parse_option_state_init(&parse_state);

    bool priv_auto_was_specified = false;
    bool priv_any_settings_were_provided = false;

    while(true) {
        arg = parse_option(&parse_state, argc, argv, &current_index, &optarg);
        if(arg == -1) {  // end of arguments
            if(!priv_auto_was_specified) {
                appconfig.privileges.enabled = priv_any_settings_were_provided;
            }
            return 0;
        }

        switch(arg) {
        case 'U':
            priv_auto_was_specified = true;
            const bool on = strcmp("on", optarg) == 0;
            const bool off = strcmp("off", optarg) == 0;
            if(on || off) {
                appconfig.privileges.enabled = on;
            } else {
                fprintf(stderr, "Argument error: --priv_auto should be 'on' or 'off' (was %s)\n", optarg);
                return -1;
            }
            break;
        case 'V':
            priv_any_settings_were_provided = true;
            appconfig.privileges.user = optarg;
            break;
        case 'W':
            priv_any_settings_were_provided = true;
            appconfig.privileges.group = optarg;
            break;
        case 'X':
            priv_any_settings_were_provided = true;
            if(!parse_capabilities_into(&appconfig.privileges.retain_capabilities, optarg)) {
                fprintf(stderr, "Argument error: failed to parse priv_cap_retain capabilities (%s)\n", optarg);
                return -1;
            }
            break;
        case 0:
            parse_argument(optarg);
            break;
        case 'a':
            if(current_index + 1 < argc) {
                return (current_index + 1);
            } else {
                return 0;
            }
            break;
        case 'c':
            appconfig.configfile = optarg;
            break;
        case 's': {
            // append so file
            string_list_iterator_t* it = string_list_iterator_createFromChar(optarg);
            string_list_append(&sofiles, it);
        }
        break;
        case 'n':
            appconfig.name = optarg;
            break;
        case 'N':
            appconfig.ns = optarg;
            break;
        case 'v':
            if(optarg) {
                appconfig.trace_level += (uint32_t) atol(optarg);
            } else {
                appconfig.trace_level += 100;
            }
            break;
        case 'f':
            appconfig.foreground = true;
            appconfig.trace_output = TRACE_TYPE_STDERR;
            break;
        case 'I':
            appconfig.ipc_socket_name = optarg;
            break;
        case 'A':
            appconfig.tcp_address = optarg;
            break;
        case 'P':
            appconfig.tcp_port = optarg;
            break;
        case 'u':
            appconfig.uri = optarg;
            break;
        case 'o':
            if(optarg) {
                if(strcmp(optarg, "syslog") == 0) {
                    appconfig.trace_output = TRACE_TYPE_SYSLOG;
                } else if(strcmp(optarg, "stdout") == 0) {
                    appconfig.trace_output = TRACE_TYPE_STDOUT;
                } else if(strcmp(optarg, "stderr") == 0) {
                    appconfig.trace_output = TRACE_TYPE_STDERR;
                }
            } else {
                appconfig.trace_output = TRACE_TYPE_SYSLOG;
            }
            break;
#ifdef OPEN_SSL_SUPPORT
        case 'C':
            appconfig.certificate = optarg;
            break;
        case 'R':
            appconfig.ca = optarg;
            break;
        case 'l':
            appconfig.ssl_connection = false;
            break;
#endif
        case 'p':
            appconfig.priority = atoi(optarg);
            break;
        case 'S':
            appconfig.sync = true;
            break;
        case 'd':
            appconfig.block_signals = false;
            break;
        case -2:
            printf("Argument error: missing argument\n\n");
            print_help(argv[0]);
            return -1;
            break;
        default:
        case 'h':
            print_help(argv[0]);
            return -1;
            break;
        }
    }

    return 0;
}
