/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdio.h>

#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <sys/prctl.h>
#include <sys/resource.h>
#include <libgen.h>

#include <components.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/utils.h>
#include <pcb/core.h>
#include <pcb/pcb_client.h>
#include <pcb/utils/privilege.h>

#include <mtk.h>

#include "parse_args.h"

typedef enum _pcb_app_type_t {
    pcb_app_application,
    pcb_app_plugin,
    pcb_app_client,
    pcb_app_mtktool,
} pcb_app_type_t;

extern plugin_config_t appconfig;
extern string_list_t sofiles;
extern argument_value_list_t arguments;

static bool exit_app = false;

static void pcb_app_signalHandler(int signal) {
    (void) signal;
    SAH_TRACE_NOTICE("Signal event handler called");
    exit_app = true;
}

static void pcb_app_load_so_files() {
    string_list_iterator_t* it = NULL;
    string_list_for_each(it, &sofiles) {
        if(!mtk_so_open(string_buffer(string_list_iterator_data(it)), NULL)) {
            SAH_TRACE_ERROR("Failed to load so: [%s]", string_buffer(string_list_iterator_data(it)));
            fprintf(stderr, "\n\n!!\n!! Process [%s] - failed to load so file [%s]\n!!\n\n", appconfig.name, string_buffer(string_list_iterator_data(it)));
        }
    }
    string_list_clear(&sofiles);
}

static void pcb_app_handleTracingChangedSetting(notification_t* notification) {
    notification_parameter_t* param = notification_firstParameter(notification);
    while(param) {
        const char* param_name = notification_parameter_name(param);
        variant_t* param_value = notification_parameter_variant(param);
        if(strcmp(param_name, "TraceLevel") == 0) {
            appconfig.trace_level = variant_uint32(param_value);
            SAH_TRACE_INFO("Change TraceLevel to %d", appconfig.trace_level);
            sahTraceSetLevel(appconfig.trace_level);
        } else if(strcmp(param_name, "Enabled") == 0) {
            if(variant_bool(param_value)) {
                sahTraceOpen(appconfig.name, appconfig.trace_output);
            } else {
                sahTraceClose();
            }
        } else {
            sah_trace_zone_it* zone = sahTraceGetZone(param_name);
            if(zone) {
                sahTraceZoneSetLevel(zone, variant_uint32(param_value));
            } else {
                sahTraceAddZone(variant_uint32(param_value), param_name);
            }
        }
        param = notification_nextParameter(param);
    }
}

static void pcb_app_handleApplicationChangeSetting(pcb_t* pcb, notification_t* notification) {
    (void) pcb;
    notification_parameter_t* group = notification_getParameter(notification, "Group");

    if(group) {
        char* value = notification_parameter_value(group);
        notification_parameter_destroy(group);
        if(strcmp(value, "Tracing") == 0) {
            // handle parameters of tracing group
            pcb_app_handleTracingChangedSetting(notification);
            free(value);
            return;
        }
        free(value);
    }

    // handle parameters not belonging to a group
    notification_parameter_t* param = notification_firstParameter(notification);
    while(param) {
        const char* param_name = notification_parameter_name(param);
        variant_t* param_value = notification_parameter_variant(param);
        if(strcmp(param_name, "EnableSync") == 0) {
            // start subscription on process
            if(variant_bool(param_value)) {
                SAH_TRACE_INFO("MTK Start");
                mtk_start(&arguments);
            } else {
                SAH_TRACE_INFO("MTK Stop");
                mtk_stop();
            }
        }

        param = notification_nextParameter(param);
    }
}

static bool pcb_app_notifyHandler(pcb_t* pcb, peer_info_t* from, notification_t* notification) {
    (void) from;
    bool ret = false;

    switch(notification_type(notification)) {
    case notify_application_change_setting:
        pcb_app_handleApplicationChangeSetting(pcb, notification);
        ret = true;
        break;
    case NOTIFY_MTK_RPC:
        SAH_TRACE_INFO("Call mtk function");
        mtk_funcHandler(notification);
        ret = true;
        break;
    }

    return ret;
}

static bool pcb_add_datamodel_notifyHandler(datamodel_t* dm, object_t* object, parameter_t* parameter, notification_t* notify) {
    (void) dm;
    (void) parameter;
    SAH_TRACE_INFO("Notification type = %d (object = %p, dm root = %p, parent = %p)", notification_type(notify), object, datamodel_root(dm), object_parent(object));
    if(object && (object_parent(object) != datamodel_root(dm))) {
        return true;
    }

    notification_t* notification = NULL;
    switch(notification_type(notify)) {
    case notify_object_added:
        SAH_TRACE_INFO("Root object %s added", object_name(object, path_attr_key_notation));
        notification = notification_createApplicationRootAdded(object_name(object, 0));
        break;
    case notify_object_deleted:
        SAH_TRACE_INFO("Root object %s deleted", object_name(object, path_attr_key_notation));
        notification = notification_createApplicationRootDeleted(object_name(object, 0));
        break;
    default:
        break;
    }

    if(notify) {
        pcb_sendNotification(appconfig.pcb, NULL, NULL, notification);
        notification_destroy(notification);
    }
    return true;
}

static bool pcb_app_connect_system() {
    if(appconfig.uri && *appconfig.uri) {
        // try command line option
        SAH_TRACE_INFO("Try to connect to %s", appconfig.uri);
        appconfig.system_bus = pcb_client_connect(appconfig.pcb, appconfig.uri);
    }

    if(!appconfig.system_bus) {
        // use ENV var PCB_SYS_BUS and try to connect
        appconfig.uri = getenv("PCB_SYS_BUS");
        if(appconfig.uri && *appconfig.uri) {
            SAH_TRACE_INFO("Try to connect to %s", appconfig.uri);
            appconfig.system_bus = pcb_client_connect(appconfig.pcb, appconfig.uri);
        }
    }

    if(!appconfig.system_bus) {
        // use ENV var PCB_SYSTEM (deprecated, use PCB_SYS_BUS) and try to connect
        char* address = getenv("PCB_SYSTEM");
        if(!address) {
            SAH_TRACE_INFO("env PCB_SYSTEM not found, using default system bus");
            address = "/var/run/pcb_sys";
        }

        SAH_TRACE_INFO("Try to connect to %s", address);
        appconfig.system_bus = connection_connectToIPC(pcb_connection(appconfig.pcb), address);
    }

    if(!appconfig.system_bus) {
        // use hardcoded default and try to connect
        appconfig.uri = "pcb://ipc:{/var/run/pcb_sys}";
        SAH_TRACE_INFO("Try to connect to %s", appconfig.uri);
        appconfig.system_bus = pcb_client_connect(appconfig.pcb, appconfig.uri);
    }

    if(!appconfig.system_bus) {
        // all failed, stop and exit
        SAH_TRACE_ERROR("Failed to connect to system bus");
        return false;
    }

    return true;
}

static bool pcb_app_create_listen_sockets() {
    uint32_t listen = 0;

    // IPC listen socket
    if(appconfig.ipc_socket_name && *appconfig.ipc_socket_name) {
        SAH_TRACE_INFO("Start listening on IPC socket %s", appconfig.ipc_socket_name);
        if(connection_listenOnIPC(pcb_connection(appconfig.pcb), appconfig.ipc_socket_name)) {
            listen++;
        } else {
            SAH_TRACE_ERROR("Failed to listen on IPC socket %s", appconfig.ipc_socket_name);
        }
    }

    // TCP listen socket
    if(appconfig.tcp_address && *appconfig.tcp_address && appconfig.tcp_port && *appconfig.tcp_port) {
        SAH_TRACE_INFO("Start listening on TCP socket %s:%s", appconfig.tcp_address, appconfig.tcp_port);
        uint32_t flags = connection_attr_default;
#ifdef OPEN_SSL_SUPPORT
        flags |= connection_attr_ssl;
#endif
        if(connection_listenOnTCP(pcb_connection(appconfig.pcb), appconfig.tcp_address, appconfig.tcp_port, flags)) {
            listen++;
        } else {
            SAH_TRACE_ERROR("Failed to listen on TCP socket %s:%s", appconfig.tcp_address, appconfig.tcp_port);
        }
    }

    return listen ? true : false;
}

static bool pcb_app_create_datamodel(pcb_app_type_t type) {
#ifdef CONFIG_PCB_ALLOW_ANONYMOUS_TCP
    // add full access for everyone on the root object
    object_aclSet(datamodel_root(pcb_datamodel(appconfig.pcb)), UINT32_MAX, acl_read | acl_write | acl_execute);
#endif

    // load definitions
    if(appconfig.configfile && *appconfig.configfile) {
        SAH_TRACE_INFO("Loading object definition file %s%s", getenv("PCB_PREFIX_PATH") ? getenv("PCB_PREFIX_PATH") : "", appconfig.configfile);
        if(!datamodel_loadDefinition(pcb_datamodel(appconfig.pcb), appconfig.configfile)) {
            SAH_TRACE_ERROR("Failed to load data model definition from file %s%s", getenv("PCB_PREFIX_PATH") ? getenv("PCB_PREFIX_PATH") : "", appconfig.configfile);
            return false;
        }
        SAH_TRACE_INFO("Data model definition loaded");
        return true;
    }

    // do not load default odl for pcb_client or mtktool
    // this is added for backwards compatibility with these
    // deprecated tools
    if((type == pcb_app_client) ||
       (type == pcb_app_mtktool)) {
        return true;
    }

    SAH_TRACE_INFO("Loading object definition file %s/usr/lib/%s/%s.odl", getenv("PCB_PREFIX_PATH") ? getenv("PCB_PREFIX_PATH") : "", appconfig.name, appconfig.name);
    char* default_config = NULL;
    if(asprintf(&default_config, "/usr/lib/%s/%s.odl", appconfig.name, appconfig.name) == -1) {
        SAH_TRACE_ERROR("Failed to allocate memory for default odl name");
    } else {
        if(!datamodel_loadDefinition(pcb_datamodel(appconfig.pcb), default_config)) {
            SAH_TRACE_NOTICE("Failed to load data model definition from file %s/usr/lib/%s/%s.odl", getenv("PCB_PREFIX_PATH") ? getenv("PCB_PREFIX_PATH") : "", appconfig.name, appconfig.name);
        } else {
            SAH_TRACE_INFO("Data model definition loaded");
        }
        free(default_config);
    }

    return true;
}

static bool pcb_app_set_event_handlers() {
    // set signal event handlers
    connection_setSignalEventHandler(pcb_connection(appconfig.pcb), SIGINT, pcb_app_signalHandler);
    connection_setSignalEventHandler(pcb_connection(appconfig.pcb), SIGTERM, pcb_app_signalHandler);
    connection_setSignalEventHandler(pcb_connection(appconfig.pcb), SIGPIPE, NULL); // ignore sigpipe

    // set notify handler
    if(!pcb_addNotifyHandler(appconfig.pcb, pcb_app_notifyHandler)) {
        SAH_TRACE_ERROR("Failed to set notify handler");
        return false;
    }

    if(!datamodel_addNotifyHandler(pcb_datamodel(appconfig.pcb), pcb_add_datamodel_notifyHandler)) {
        SAH_TRACE_ERROR("Failed to set notify handler");
        pcb_removeNotifyHandler(appconfig.pcb, pcb_app_notifyHandler);
        return false;
    }

    return true;
}

static bool pcb_app_daemonize() {
    if(!appconfig.foreground) {
        SAH_TRACE_INFO("daemonize pcb app");
        if(daemon(1, 0) == -1) {
            SAH_TRACE_ERROR("Failed to daemonize (%d)", errno);
            return false;
        }
    }

    return true;
}

static void pcb_app_set_app_name(char* orig_name) {
    if(appconfig.name && *appconfig.name) {
        char name[17];
        name[16] = 0;
        strncpy(name, appconfig.name, 16);
        strncpy(orig_name, name, strlen(orig_name));
        if(prctl(PR_SET_NAME, (unsigned long) name, 0, 0, 0) < 0) {
            SAH_TRACE_ERROR("unable to change name: (%d)", errno);
        }
    }
}

static char* pcb_app_create_pid_file(pid_t pid) {
    string_t pidfile;
    string_initialize(&pidfile, 64);

    // create pidfile
    string_fromChar(&pidfile, "/var/run/");
    string_appendChar(&pidfile, appconfig.name);
    string_appendChar(&pidfile, ".pid");
    FILE* pf = fopen(string_buffer(&pidfile), "w");
    if(!pf && (errno == EACCES)) {
        // permission is denied, try if access is allowed to /var/run/appname/appname.pid
        SAH_TRACE_ERROR("Try subsir");
        string_fromChar(&pidfile, "/var/run/");
        string_appendChar(&pidfile, appconfig.name);
        string_appendChar(&pidfile, "/");
        string_appendChar(&pidfile, appconfig.name);
        string_appendChar(&pidfile, ".pid");
        pf = fopen(string_buffer(&pidfile), "w");
    }
    if(pf) {
        fprintf(pf, "%d", pid);
        fflush(pf);
        fclose(pf);
    } else {
        SAH_TRACE_ERROR("Failed to create pidfile %s for %s - %s", string_buffer(&pidfile), appconfig.name, strerror(errno));
    }

    return (char*) string_buffer(&pidfile);
}

static void pcb_app_flush_peers() {
    if(!appconfig.pcb) {
        return;
    }

    llist_t cons;
    llist_initialize(&cons);
    llist_append(&cons, connection_getIterator(pcb_connection(appconfig.pcb)));

    fd_set readset;
    fd_set writeset;

    connection_info_t* info = pcb_connection(appconfig.pcb);
    connection_t* conn;
    int ret = 0;
    do {
        for(conn = (connection_t*) llist_first(&info->connections); conn; conn = (connection_t*) llist_iterator_next(&conn->it)) {
            if(peer_needWrite(&conn->info)) {
                SAH_TRACE_INFO("One socket still needs to flush data");
                break;
            }
        }
        if(!conn) {
            SAH_TRACE_INFO("all buffers flushed");
            break;
        }
        ret = connection_waitForEvents_r(&cons, NULL, &readset, &writeset);
        if(ret > 0) {
            ret = connection_handleEvents_r(&cons, &readset, &writeset) ? 1 : 0;
        }
    } while(ret > 0);
    llist_cleanup(&cons);
}

static bool pcb_app_initialize(pcb_app_type_t type) {
    SAH_TRACE_IN();
    // initialize pcb
    //////////////////////////////////////////
    int listen = 0;

    // if signals shouldn't be blocked, do it now
    if(appconfig.block_signals == false) {
        connection_blockSignals(false);
    }

#ifdef OPEN_SSL_SUPPORT
    SAH_TRACE_INFO("SSL support is enabled");
    connection_enableSSL(true);
#else
    SAH_TRACE_INFO("SSL support is disabled");
    connection_enableSSL(false);
#endif

    connection_initLibrary();

    // create the connection object
    appconfig.pcb = pcb_create(appconfig.name, appconfig.argc, appconfig.argv);
    if(!appconfig.pcb) {
        SAH_TRACE_ERROR("Failed to create connection object");
        goto error;
    }

    // set the application configuration to the pcb context
    pcb_setPluginConfig(appconfig.pcb, &appconfig);

    // set the default serializer format - needed to be able to send pcb messages
    if(!pcb_setDefaultFormat(appconfig.pcb, SERIALIZE_FORMAT(serialize_format_default, SERIALIZE_DEFAULT_MAJOR, SERIALIZE_DEFAULT_MINOR))) {
        SAH_TRACE_ERROR("Failed to find serialization format");
        goto error_cleanup_connection;
    }

#if OPEN_SSL_SUPPORT
    if(appconfig.ssl_connection) {
        if(!connection_setupSSL(pcb_connection(appconfig.pcb), appconfig.ca, appconfig.certificate, ssl_attribute_default)) {
            SAH_TRACE_ERROR("Failed to set certificate file");
            goto error_cleanup_connection;
        }
    }
#endif

    // Connect to the pcb bus
    if(pcb_app_connect_system()) {
        listen++;
    } else {
        SAH_TRACE_NOTICE("Failed to connect to the system bus");
    }

    // create the listen sockets
    if(pcb_app_create_listen_sockets()) {
        listen++;
    } else {
        SAH_TRACE_NOTICE("No listen sockets created");
    }

    // Check that at least one listen socket is available or a connection to the system bus has been made
    // otherwise just exit the application
    if(!listen) {
        SAH_TRACE_ERROR("No listen sockets available and no connection to the system bus, stop");
        goto error_cleanup_connection;
    }

    string_t mibdir;
    string_initialize(&mibdir, 64);
    string_appendFormat(&mibdir, "/usr/lib/%s/mibs/", appconfig.name);
    SAH_TRACE_INFO("Setting mib dir to [%s]", string_buffer(&mibdir));
    datamodel_setMibDir(pcb_datamodel(appconfig.pcb), string_buffer(&mibdir));
    string_cleanup(&mibdir);

    // create and initialize the data model
    if(!pcb_app_create_datamodel(type)) {
        SAH_TRACE_ERROR("Failed to create the data model");
        goto error_cleanup_connection;
    }

    // set the event handlers
    if(!pcb_app_set_event_handlers()) {
        SAH_TRACE_ERROR("Failed to set the event handlers");
        goto error_cleanup_connection;
    }

    // set the namespace
    datamodel_setNamespace(pcb_datamodel(appconfig.pcb), appconfig.ns);

    // now the app is initialize and can be daemonized
    SAH_TRACE_INFO("pcb_app initialized");
    pcb_app_daemonize();

    // NOTE:
    // mtk_init can only be done after daemonizing
    // for each request that is send, the PID is included
    // daemonizing willl change the PID

    // initialize mtk
    mtk_init(&appconfig, &arguments);
    SAH_TRACE_INFO("Bus name = [%s]", appconfig.busname);
    // load so files here
    pcb_app_load_so_files();

    return true;

error_cleanup_connection:
    pcb_destroy(appconfig.pcb);
    appconfig.pcb = NULL;

error:
    fprintf(stdout, "pcb app (%s) failed to initialize\n", appconfig.name);
    SAH_TRACE_ERROR("pcb app (%s) failed to initialize", appconfig.name);
    return false;
}

static int pcb_app_eventloop() {
    int retval = 0;

    SAH_TRACE_NOTICE("%s entered event loop", appconfig.name);

    llist_t cons;
    llist_initialize(&cons);
    llist_append(&cons, connection_getIterator(pcb_connection(appconfig.pcb)));

    fd_set readset;
    fd_set writeset;

    while(!exit_app) {
        // call select
        retval = connection_waitForEvents_r(&cons, NULL, &readset, &writeset);
        if(retval < 0) {
            // an error has happened
            SAH_TRACE_ERROR("Error %d (%s)", pcb_error, error_string(pcb_error));
            continue;
        }

        if(retval > 0) {
            // events available (signals or fd events)
            connection_handleEvents_r(&cons, &readset, &writeset);
        }
    }

    llist_cleanup(&cons);
    SAH_TRACE_NOTICE("%s leaving event loop", appconfig.name);
    return retval;
}

static void pcb_app_cleanup() {
    SAH_TRACE_INFO("Stopping mtk");
    mtk_unload();

    SAH_TRACE_INFO("Send stopping notification");
    notification_t* notification = notification_createApplicationStopping(appconfig.name);
    if(notification) {
        if(appconfig.busname) {
            notification_parameter_t* notif_param = notification_parameter_create("BusName", appconfig.busname);
            notification_addParameter(notification, notif_param);
        }
        pcb_sendNotification(appconfig.pcb, appconfig.system_bus, NULL, notification);
        notification_destroy(notification);
    }

    datamodel_removeNotifyHandler(pcb_datamodel(appconfig.pcb), pcb_add_datamodel_notifyHandler);
    pcb_removeNotifyHandler(appconfig.pcb, pcb_app_notifyHandler);

    SAH_TRACE_INFO("Flush peers");
    pcb_app_flush_peers();

    SAH_TRACE_INFO("Cleanup pcb context");
    pcb_destroy(appconfig.pcb);
    appconfig.pcb = NULL;

    SAH_TRACE_INFO("Cleanup bus name");
    appconfig.busname = NULL;

    SAH_TRACE_INFO("Cleanup mtk");
    mtk_cleanup();

    SAH_TRACE_INFO("Cleanup library");
    connection_exitLibrary();

    SAH_TRACE_INFO("Cleanup done");
}

static int handle_privilege_settings(const privileges_config_t* privileges) {

    if(!privileges->enabled) {
        return 0;
    }

    if(privileges->user && !priv_set_defaultUser(privileges->user)) {
        SAH_TRACE_ERROR("Failed to set default user '%s'", privileges->user);
        return -1;
    }
    if(privileges->group && !priv_set_defaultGroup(privileges->group)) {
        SAH_TRACE_ERROR("Failed to set default group '%s'", privileges->group);
        return -1;
    }
    if(!priv_set_defaultRetain(&privileges->retain_capabilities)) {
        SAH_TRACE_ERROR("Failed to set retain_capabilities");
        return -1;
    }

    if(!priv_proc_dropPrivileges(priv_get_defaultUser(),
                                 priv_get_defaultGroup(),
                                 priv_get_defaultRetain())) {
        return -1;
    }

    return 0;
}

int main(int argc, char* argv[]) {
    int retval = 0;
    bool createPid = false;
    int index = parse_arguments(argc, argv);
    pid_t pid = 0;
    char* pidfile = NULL;

    if(index < 0) {
        return -1;
    }
    // initialize tracing and logging
    sahTraceOpen(appconfig.name, appconfig.trace_output);
    if(appconfig.trace_level) {
        sahTraceSetLevel(appconfig.trace_level);
    }
    SAH_TRACE_INFO("Initialize priv_proc library? %s", appconfig.privileges.enabled ? "Yes" : "No");
    if(appconfig.privileges.enabled) {
        if(!priv_proc_initialize()) {
            SAH_TRACE_ERROR("Failed to initialize privilege library");
            goto error_exit;
        }
    }

    const char* app = basename(argv[0]);
    pcb_app_type_t type = pcb_app_application;
    SAH_TRACE_INFO("Application started = %s", app);
    if(strcmp(app, "pcb_plugin") == 0) {
        type = pcb_app_plugin;
    } else if(strcmp(app, "pcb_client") == 0) {
        type = pcb_app_client;
    } else if(strcmp(app, "mtktool") == 0) {
        type = pcb_app_mtktool;
    }

    // change the name of the application
    pcb_app_set_app_name(argv[0]);

    // search start of plug-in parameters
    if(index > 0) {
        appconfig.argc = argc - index;
        appconfig.argv = &(argv[index]);
        SAH_TRACE_INFO("Arguments start at index %d (%s)", index, appconfig.argv[0]);
    } else {
        SAH_TRACE_INFO("Arguments: none");
    }

    // if no name was specified, use the application name
    if(!strlen(appconfig.name)) {
        appconfig.name = argv[0];
    } else {
        createPid = true;
    }

    // initialize
    if(!pcb_app_initialize(type)) {
        goto error_exit;
    }

    pid = getpid();
    // set the nice level
    if(setpriority(PRIO_PROCESS, pid, appconfig.priority) == -1) {
        SAH_TRACE_ERROR("Failed to set nice level (%d)", errno);
    }

    // create a pid file, if a name was set and
    // the process is daemonized
    pidfile = NULL;
    if(createPid) {
        pidfile = pcb_app_create_pid_file(pid);
    }

    // drop privileges
    if(handle_privilege_settings(&appconfig.privileges)) {
        SAH_TRACE_ERROR("Failed to use provided privilege arguments (%d)", errno);
    }

    // all initialization done,
    // start the eventloop
    SAH_TRACE_INFO("Started");
    retval = pcb_app_eventloop();
    SAH_TRACE_INFO("Stopping");

    // remove the pid file
    if(createPid) {
        unlink(pidfile);
    }

    free(pidfile);

    // cleanup
    pcb_app_cleanup();

    SAH_TRACE_INFO("Cleanup priv_proc library? %s", appconfig.privileges.enabled ? "YES" : "NO");
    if(appconfig.privileges.enabled) {
        priv_proc_cleanup();
    }

    SAH_TRACE_INFO("Close tracing");
    sahTraceClose();
    SAH_TRACE_INFO("Exit %s", appconfig.name);
    return retval;

error_exit:
    pcb_app_cleanup();
    // close traces
    SAH_TRACE_INFO("Closing traces");
    sahTraceClose();
    return -1;
}
