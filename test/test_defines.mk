MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../normal/src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../normal/include_priv)

LD_LIB=LD_LIBRARY_PATH=$(LD_LIBRARY_PATH)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
	  --std=gnu99 -g3 \
		-I$(INCDIR) \
		-fkeep-inline-functions -fkeep-static-functions -Wno-format-nonliteral \
		-pthread -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500 \
		$(shell pkg-config --cflags cmocka sahtrace pcb test-toolbox )

CFLAGS  += -fPIC -Wmissing-prototypes -I$(STAGINGDIR)/include -I$(STAGINGDIR)/usr/include

LDFLAGS += -L$(STAGINGDIR)/lib \
	   -L$(STAGINGDIR)/usr/lib

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		-Wl,--no-undefined -Wl,--error-unresolved-symbols \
		$(shell pkg-config --libs cmocka sahtrace pcb test-toolbox)
