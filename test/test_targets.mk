all: $(TARGET)

run: $(TARGET)
	/usr/lib/libtest-toolbox/ttb_pcb-sysbus.sh start
	set -o pipefail; $(LD_LIB) valgrind --leak-check=full --show-leak-kinds=all --keep-debuginfo=yes --error-exitcode=1 ./$< 2>&1 | tee -a $(OBJDIR)/unit_test_results.txt;
	/usr/lib/libtest-toolbox/ttb_pcb-sysbus.sh stop

$(TARGET): $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) ../../output/$(MACHINE)/parse_args.o $(LDFLAGS) -fprofile-arcs -ftest-coverage

-include $(OBJECTS:.o=.d)

$(OBJDIR)/%.o: ./%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/%.o:

$(OBJDIR)/:
	mkdir -p $@

clean:
	rm -rf $(TARGET) $(OBJDIR)

.PHONY: clean $(OBJDIR)/
