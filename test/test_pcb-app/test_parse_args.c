#include "parse_args.h"

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <pcb/utils/privilege.h>
#include <pcb/utils/string_list.h>

#include <debug/sahtrace.h>

#include "wrap.h"

extern plugin_config_t appconfig;
extern string_list_t sofiles;

static void when_nothing_is_provided_privileges_are_off(void** state) {
    (void) state;
    char* args[] = {"argv[0]"};
    assert_int_equal(0, parse_arguments(1, args));
    assert_false(appconfig.privileges.enabled);
}

static void appconfig_is_cleared_at_each_parse_run(void** state) {
    (void) state;
    char* args[] = {"argv[0]"};

    plugin_config_t cleared;
    clear_config(&cleared);

    // make it dirty
    appconfig.priority = 100;
    const char* sofile = "abcdad.so";
    appconfig.sofile = sofile;

    assert_int_equal(0, parse_arguments(1, args));

    assert_memory_equal(&cleared, &appconfig, sizeof(cleared));
}

static void any_privilege_argument_causes_privileges_enabled_impl(char* argument) {
    clear_config(&appconfig);
    assert(!appconfig.privileges.enabled);
    char* args[] = {"argv[0]", argument, "mandatory.so"};
    assert_int_equal(0, parse_arguments(2, args));
    assert_true(appconfig.privileges.enabled);
}

static void any_privilege_argument_causes_privileges_enabled(void** state) {
    (void) state;
    any_privilege_argument_causes_privileges_enabled_impl("--priv_user=ME");
    any_privilege_argument_causes_privileges_enabled_impl("--priv_group=ME");
    any_privilege_argument_causes_privileges_enabled_impl("--priv_cap_retain=CAP_CHOWN,CAP_SYS_ADMIN,CAP_SYS_TIME");
}

static void priv_user_is_read_from_commandline(void** state) {
    (void) state;
    char* args[] = {"argv[0]", "--priv_user=ME", "mandatory.so"};
    assert_int_equal(0, parse_arguments(2, args));
    assert(appconfig.privileges.enabled);

    assert_non_null(appconfig.privileges.user);
    assert_string_equal(appconfig.privileges.user, "ME");
}

static void priv_group_is_read_from_commandline(void** state) {
    (void) state;
    char* args[] = {"argv[0]", "--priv_group=ME", "mandatory.so"};
    assert_int_equal(0, parse_arguments(2, args));
    assert(appconfig.privileges.enabled);

    assert_non_null(appconfig.privileges.group);
    assert_string_equal(appconfig.privileges.group, "ME");
}

static void priv_stuff_cannot_be_forced_to_oneedatwilikniet(void** state) {
    (void) state;
    char* args[] = {"argv[0]", "--priv_user=ME", "--priv_auto=oneedatwilikniet", "mandatory.so"};
    assert_int_not_equal(0, parse_arguments(4, args));
}

static void priv_stuff_can_be_forced_off(void** state) {
    (void) state;
    char* args[] = {"argv[0]", "--priv_user=ME", "--priv_auto=off", "mandatory.so"};
    assert_int_equal(0, parse_arguments(4, args));
    assert_false(appconfig.privileges.enabled);

    string_list_clear(&sofiles);
}


// mock: just return the first char
uint32_t (* __wrap_priv_cap_fromString__current)(const char* cap_str);
/*
   uint32_t __wrap_priv_cap_fromString(const char* cap_str) {
    return __wrap_priv_cap_fromString__current(cap_str);
   }
 */
static uint32_t __wrap_priv_cap_fromString__constant_result = 0b0;
uint32_t __wrap_priv_cap_fromString__constant(const char* cap_str UNUSED) {
    return __wrap_priv_cap_fromString__constant_result;
}
uint32_t __wrap_priv_cap_fromString__ABC_123(const char* cap_str) {
    return cap_str[0] - 'A' + 1;
}

bool (* __wrap_priv_cap_set__current)(priv_cap_t* capt, uint32_t cap);
bool __wrap_priv_cap_set(priv_cap_t* capt, uint32_t cap) {
    return __wrap_priv_cap_set__current(capt, cap);
}
static priv_cap_t __wrap_priv_cap_set__constant_capt = {0};
static bool __wrap_priv_cap_set__constant_result = true;
bool __wrap_priv_cap_set__constant(priv_cap_t* capt, uint32_t cap UNUSED) {
    memcpy(capt, &__wrap_priv_cap_set__constant_capt, sizeof(__wrap_priv_cap_set__constant_capt));
    return __wrap_priv_cap_set__constant_result;
}
bool __wrap_priv_cap_set__from_mock(priv_cap_t* capt, uint32_t cap) {
    priv_cap_t* value = (priv_cap_t*) mock();
    check_expected(cap);
    memcpy(capt, value, sizeof(__wrap_priv_cap_set__constant_capt));
    return true;
}
/*
 #define push_mock_value will_return

   uint32_t mock_priv_cap_fromString(const char* cap_str);

   const priv_cap_t cap_set_result_ABC_123 = {{0b101010, 0b000000}};

   static void prepare_ABC_123() {
    __wrap_priv_cap_fromString__current = __wrap_priv_cap_fromString__ABC_123;
    __wrap_priv_cap_set__current = __wrap_priv_cap_set__from_mock;
    const priv_cap_t cap_set_result1 = {{0b000010, 0b000000}};
    expect_value(__wrap_priv_cap_set__from_mock, cap, 1);
    push_mock_value(__wrap_priv_cap_set__from_mock, &cap_set_result1);
    const priv_cap_t cap_set_result2 = {{0b001010, 0b000000}};
    expect_value(__wrap_priv_cap_set__from_mock, cap, 2);
    push_mock_value(__wrap_priv_cap_set__from_mock, &cap_set_result2);
    const priv_cap_t* cap_set_result3 = &cap_set_result_ABC_123;
    expect_value(__wrap_priv_cap_set__from_mock, cap, 3);
    push_mock_value(__wrap_priv_cap_set__from_mock, cap_set_result3);
   }
 */

static void prepare_no_capabilities() {
    __wrap_priv_cap_fromString__current = __wrap_priv_cap_fromString__constant;
}

static void priv_retain_capabilities_are_read_from_commandline(void** state) {
    (void) state;
    char* args[] = {"argv[0]", "--priv_cap_retain=CAP_CHOWN,CAP_SYS_ADMIN,CAP_SYS_TIME", "mandatory.so"};

    // given
    //prepare_ABC_123();

    // when
    assert_int_equal(0, parse_arguments(2, args));

    // then
    //assert_memory_equal(&appconfig.privileges.retain_capabilities, &cap_set_result_ABC_123, sizeof(cap_set_result_ABC_123));
}

static void priv_unknown_retain_capabilities_cause_an_error(void** state) {
    (void) state;
    char* args[] = {"argv[0]", "--priv_cap_retain=A,B,C", "mandatory.so"};

    // given
    __wrap_priv_cap_fromString__current = __wrap_priv_cap_fromString__constant;
    __wrap_priv_cap_fromString__constant_result = 0xFFFFFFFF;

    assert_int_equal(-1, parse_arguments(2, args));
}


static void priv_all_settings_are_read_from_commandline(void** state) {
    (void) state;
    char* args[] = {"argv[0]",
        "--priv_group=ME",
        "--priv_user=YOU",
        "--priv_cap_retain=CAP_CHOWN,CAP_SYS_ADMIN,CAP_SYS_TIME",
        "mandatory.so"};
    //prepare_ABC_123();

    assert_int_equal(0, parse_arguments(sizeof(args) / sizeof(args[0]), args));

    assert_true(appconfig.privileges.enabled);
    assert_string_equal(appconfig.privileges.user, "YOU");
    assert_string_equal(appconfig.privileges.group, "ME");
    //assert_memory_equal( &appconfig.privileges.retain_capabilities, &cap_set_result_ABC_123, sizeof(cap_set_result_ABC_123));

    string_list_clear(&sofiles);
}

static void priv_cap_retain_can_be_empty(void** state) {
    (void) state;
    char* args[] = {"argv[0]",
        "--priv_group=ME",
        "--priv_user=YOU",
        "--priv_cap_retain=",
        "mandatory.so"};

    const priv_cap_t cap_set_result_empty = {0b0};
    prepare_no_capabilities();

    assert_int_equal(0, parse_arguments(sizeof(args) / sizeof(args[0]), args));

    assert_true(appconfig.privileges.enabled);
    assert_string_equal(appconfig.privileges.user, "YOU");
    assert_string_equal(appconfig.privileges.group, "ME");
    assert_memory_equal(
        &appconfig.privileges.retain_capabilities,
        &cap_set_result_empty,
        sizeof(cap_set_result_empty));

    string_list_clear(&sofiles);
}

static int parse_args_test_setup(void** state UNUSED) {
    clear_config(&appconfig);

    __wrap_priv_cap_fromString__constant_result = 0;
    __wrap_priv_cap_fromString__current = __wrap_priv_cap_fromString__constant;

    memset(&__wrap_priv_cap_set__constant_capt, 0, sizeof(__wrap_priv_cap_set__constant_capt));
    __wrap_priv_cap_set__current = __wrap_priv_cap_set__constant;
    return 0;
}

int main(int argc UNUSED, char** argv UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "privilege");

    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(when_nothing_is_provided_privileges_are_off, parse_args_test_setup),
        cmocka_unit_test_setup(appconfig_is_cleared_at_each_parse_run, parse_args_test_setup),
        cmocka_unit_test_setup(any_privilege_argument_causes_privileges_enabled, parse_args_test_setup),
        cmocka_unit_test_setup(priv_user_is_read_from_commandline, parse_args_test_setup),
        cmocka_unit_test_setup(priv_group_is_read_from_commandline, parse_args_test_setup),
        cmocka_unit_test_setup(priv_all_settings_are_read_from_commandline, parse_args_test_setup),
        cmocka_unit_test_setup(priv_stuff_can_be_forced_off, parse_args_test_setup),
        cmocka_unit_test_setup(priv_stuff_cannot_be_forced_to_oneedatwilikniet, parse_args_test_setup),
        cmocka_unit_test_setup(priv_retain_capabilities_are_read_from_commandline, parse_args_test_setup),
        cmocka_unit_test_setup(priv_cap_retain_can_be_empty, parse_args_test_setup),
        cmocka_unit_test_setup(priv_unknown_retain_capabilities_cause_an_error, parse_args_test_setup),
    };
    int retval = cmocka_run_group_tests(tests, NULL, NULL);
    sahTraceClose();
    return retval;
}
