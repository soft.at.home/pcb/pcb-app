/****************************************************************************
**
** Copyright (c)  2023 SoftAtHome
**
** The information and source code contained herein is the exclusive
** property of SoftAtHome and may not be disclosed, examined, or
** reproduced in whole or in part without explicit written authorization
** of the copyright owner.
**
****************************************************************************/

#ifndef __TEST_WRAP_H__
#define __TEST_WRAP_H__

#include <stdint.h>

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

uint32_t __wrap_priv_cap_fromString(const char* cap_str);
uint32_t __wrap_priv_cap_fromString__constant(const char* cap_str);
uint32_t __wrap_priv_cap_fromString__ABC_123(const char* cap_str);
bool __wrap_priv_cap_set(priv_cap_t* capt, uint32_t cap);
bool __wrap_priv_cap_set__constant(priv_cap_t* capt, uint32_t cap);
bool __wrap_priv_cap_set__from_mock(priv_cap_t* capt, uint32_t cap);

#endif //__TEST_WRAP_H__
