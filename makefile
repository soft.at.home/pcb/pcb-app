-include $(CONFIGDIR)/components.config

export INSTALL ?= install
export PKG_CONFIG_LIBDIR ?= /usr/lib/pkgconfig
export BINDIR ?= /usr/bin
export LIBDIR ?= /usr/lib
export SLIBDIR ?= /usr/lib
export LUALIBDIR ?= /usr/lib/lua
export INCLUDEDIR ?= /usr/include
export INITDIR ?= /etc/init.d
export ACLDIR ?= /etc/acl
export DOCDIR ?= $(D)/usr/share/doc/pcb-app
export PROCMONDIR ?= /usr/lib/processmonitor/scripts
export RESETDIR ?= /etc/reset
export MACHINE ?= $(shell $(CC) -dumpmachine)

export COMPONENT = pcb-app

compile:
	$(MAKE) -C normal/src all
	$(MAKE) -C ssl/src all

clean:
	$(MAKE) -C normal/src clean
	$(MAKE) -C ssl/src clean

install:
	$(INSTALL) -D -p -m 0755 normal/src/pcb_app $(D)/bin/pcb_app
	$(INSTALL) -D -p -m 0755 ssl/src/pcbs_app $(D)/bin/pcbs_app
	$(INSTALL) -d $(D)/bin
	ln -sfr $(D)/bin/pcb_app $(D)/bin/pcb_client
	$(INSTALL) -d $(D)/bin
	ln -sfr $(D)/bin/pcb_app $(D)/bin/mktool
	$(INSTALL) -d $(D)/bin
	ln -sfr $(D)/bin/pcb_app $(D)/bin/pcb_plugin
	$(INSTALL) -d $(D)/bin
	ln -sfr $(D)/bin/pcbs_app $(D)/bin/pcbs_plugin

.PHONY: compile clean install

