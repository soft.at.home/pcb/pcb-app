#!/bin/bash

cat <<EOF
WARNING: keep this file in sync with the
dependencies declared in .gitlab-ci.yml:
EOF

DEPS_STRING=$(
    sed -n -e "/COMPONENT_DEPS:/,/^$/p"  .gitlab-ci.yml \
    | tail -n +2 \
    | tr '\n' ' ' \
)

COMPONENT_OPENSSL_DIR=${COMPONENT_OPENSSL_DIR-gen_1.0.2k}
COMPONENT_DEPS_DIR=${COMPONENT_DEPS_DIR-master}
DEPS=$(eval echo $DEPS_STRING)

install_packages.sh /tmp $DEPS
